### Hệ thống quản lý camera 

> **Mô tả**: Hệ thống có chức năng chính là giám sát nhiều camera streaming online 1 lúc và hỗ trợ xem lại video lưu trữ của từng camera. 


| Prototype | App Developed |
|-----------|---------------|
|![home_page](Prototype/1.png) | ![home_page](Prototype/1_real.png)|
|-----------|---------------|
|![home_page](Prototype/2.png) | ![home_page](Prototype/2_real.png)|
|-----------|---------------|
|![home_page](Prototype/3.png) | ![home_page](Prototype/3_real.png)|
|-----------|---------------|
|![home_page](Prototype/4.png) | ![home_page](Prototype/4_real.png)|
|-----------|---------------|
|![home_page](Prototype/5.png) | ![home_page](Prototype/5_real.png)|
|-----------|---------------|
|![home_page](Prototype/6.png) | ![home_page](Prototype/6_real.png)|
