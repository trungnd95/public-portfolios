### Hệ thống tác chiến chia sẻ hình ảnh UAV

> **Mô tả**: Hệ thống bao gồm 3 thành phần: phần mềm payload nhận và xử lý hình ảnh trên UAV, phần mềm điều khiển mặt đất GCS (sở chỉ huy GCS), ứng dụng mobile cho việc chia sẻ hình ảnh.
 
> Hình ảnh/dữ liệu từ camera (payload) UAV truyền về phần mềm sở chỉ huy (GCS) bằng giao thức UDP/TCP thông qua hệ thống thông tin telemetry. GCS kết nối và lựa chọn chia sẻ hình ảnh đến các người dùng app mobile chính hình ảnh nhận được từ phía UAV qua 1 web server. Điều này giúp cho ng dùng app mobile có thể quan sát được hình ảnh UAV, hỗ trợ quá trình thực hiện nhiệm vụ. Ngoài ra, ng dùng app mobile có thể chia sẻ ngược lại hình ảnh thực tế tại thực địa làm nhiệm vụ đến sở chỉ huy, hỗ trợ sở chỉ huy phản ứng nhanh trong việc ra quyết định. 

##### - Giao diện mobile app
- Mobile app icon

![Mobile_app_icon](Prototype/0.Mobile_app_icon.png)

- Mobile app kết nối đến server (auto first then manual if not found)

![Mobile_app_connect_to_server](Prototype/1.Giao_diện_mobile_app_kết_nối_đến_server.png)

![Mobile_app_manual_connect_to_server](Prototype/2.Giao_diện_mobile_app_nhập_địa_chỉ_server.png)

![Mobile_app_connect_error](Prototype/3.Giao_diện_mobile_kết_nối_lỗi.png)

![Mobile_app_list_server_found](Prototype/4.Giao_diện_mobile_danh_sách_các_servers.png)

- Mobile app đăng nhập vào hệ thống

![Mobile_app_login_portal](Prototype/5.Giao_diện_mobiel_đăng_nhập_vào_server.png)

- Mobile app pages / use-cases

	- Homepage

	![Mobile_homepage](Prototype/Giao_diện_mobile_trang_chủ.png)
	
	- Trang hiển thị danh sách uavs kết nối

	![Mobile_uav_list](Prototype/7.Giao_diện_mobile_ds_uavs.png)

	- Trang hiển thị danh sách người dùng đang online trong hệ thống

	![Mobile_users_list](Prototype/9.Giao_diện_mobile_ds_người_dùng_online.png)

	- Trang hiển thị vị trí người dùng hiện tại trên bản đồ

	![Mobile_user_location](Prototype/8.Giao_diện_mobile_vị_trí_trên_bản_đồ.png)

	- Use-case nhận chia sẻ video từ uav thông qua sở chỉ huy GCS	

	![Mobile_video_sharing](Prototype/6.Giao_diện_mobile_yc_chia_sẻ_hình_ảnh.png)

	![Mobile_video_sharing](Prototype/Giao_diện_mobile_nhận_hình_ảnh_từ_uav.png)

	- Use-case yêu cầu nhận hình ảnh từ UAV

	![Mobile_video_sharing_request](Prototype/Giao_diện_mobile_chờ_accept_nhận_hình_ảnh.png)

	- Use-case nhận hình ảnh từ người dùng khác

	![Mobile_video_sharing_from_other](Prototype/Giao_diện_mobile_nhận_hình_ảnh_từ_2_nguồn.png)
	
	- Chatroom

	![Chatroom](Prototype/Giao_diện_mobile_chat.png)
	
	- Thông báo lỗi

	![Mobile_error_alert](Prototype/Giao_diện_mobile_thông_báo_lỗi.png)
	
	![Mobile_error_alert](Prototype/smartmockups_k9wey3fn.png)

	![Mobile_error_alert](Prototype/Giao_diện_mobile_nhận_hình_ảnh_từ_2_nguồn_.png)

##### - Giao diện sở chỉ huy GCS
- Danh sách người dùng để chia sẻ hình ảnh

![GCS_homepage](Prototype/Giao_diện_GCS_1.png)

- Nếu người dùng đã được chia sẻ hình ảnh thì không thể nhấn chia sẻ.

![GCS](Prototype/Giao_diện_GCS_2.png)

- Khi có người dùng yêu cầu chia sẻ hình ảnh

![GCS](Prototype/Giao_diện_GCS_khi_có_người_dùng_yc_truy_cập_ha.png)

- Nhận hình ảnh chia sẻ từ người dùng

![GCS](Prototype/Giao_diện_GCS_khi_có_ng_dùng_request_chia_sẻ_ha.png)

![GCS](Prototype/Giao_diện_GCS_nhận_ha_chia_sẻ_từ_ng_dùng.png)

- Khi người dùng mất kết nối 

![GCS](Prototype/Giao_diện_GCS_khi_ng_dùng_mất_kết_nối.png)

> Note: Người dùng ở đây là các app mobile.

