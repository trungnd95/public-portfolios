### Hệ thống streaming camera trực tuyến

> **Mô tả**: Hệ thống bao gồm 2 thành phần: phía server có chức năng đọc hình ảnh từ camera, nén hình ảnh để gửi đi; phía client có nhiệm vụ nhận hình ảnh từ UDP, giải nén và hiển thị. Ngoài ra, 2 thành phần còn giao tiếp với nhau qua TCP socket để cấu hình các tham số. 


##### Prototype

![app_navigator](Prototype/1.PNG)

![auto_connect_server](Prototype/2.PNG)

![auto_connect_success](Prototype/3.PNG)

![config_board](Prototype/4.PNG)

![steraming_board](Prototype/5.PNG)
